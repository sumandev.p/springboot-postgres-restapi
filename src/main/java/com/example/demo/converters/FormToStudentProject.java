package com.example.demo.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.example.demo.commands.Form;
import com.example.demo.domain.StudentProject;

@Component
public class FormToStudentProject implements Converter<Form, StudentProject> {
	
	 @Override
	    public StudentProject convert(Form form) {
	      
	        StudentProject studentProject = new StudentProject();
	        if (form.getId() != null  && !StringUtils.isEmpty(form.getId())) {
	            studentProject.setId(new Long(form.getId()));
	        }
	    
	        studentProject.setProject_name(form.getProject_name());
	         studentProject.setProject_description(form.getProject_description());	     
	         studentProject.setInstitution(form.getInstitution());
	        return studentProject;
	    }

}
