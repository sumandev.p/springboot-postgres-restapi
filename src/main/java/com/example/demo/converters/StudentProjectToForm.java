package com.example.demo.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.example.demo.commands.Form;
import com.example.demo.domain.StudentProject;

@Component
public class StudentProjectToForm implements Converter<StudentProject, Form> {
	
	@Override
    public Form convert(StudentProject studentProject) {
      //  ProductForm productForm = new ProductForm();
		Form form = new Form();
        form.setId(studentProject.getId());
        form.setProject_name(studentProject.getProject_name());        
        form.setProject_description(studentProject.getProject_description());
        form.setInstitution(studentProject.getInstitution());
        return form;
    }
}
