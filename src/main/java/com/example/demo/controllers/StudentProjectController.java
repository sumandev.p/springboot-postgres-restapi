package com.example.demo.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.commands.Form;
import com.example.demo.converters.StudentProjectToForm;
import com.example.demo.domain.StudentProject;
import com.example.demo.services.StudentProjectService;

@RestController
public class StudentProjectController {
	
	private StudentProjectService studentProjectService;
	private StudentProjectToForm studentProjectToForm;
	
	
	@Autowired
	public void setStudentProjectService(StudentProjectService studentProjectService) {
		this.studentProjectService = studentProjectService;
	}
	@Autowired
	public void setStudentProjectToForm(StudentProjectToForm studentProjectToForm) {
		this.studentProjectToForm = studentProjectToForm;
	}
	
	 @RequestMapping("/")
	    public String redirToList(){
	        return "redirect:/studentproject/list";
	    }
	 
	 @RequestMapping({"/studentproject/list", "/studentproject"})
	    public List<StudentProject> listProducts(Model model){
	        model.addAttribute("studentprojects", studentProjectService.listAll());
	        return studentProjectService.listAll();
	    }
	 
	 
	 @RequestMapping("/studentproject/admin/list")
	    public List<StudentProject> listProductsall(){
	        
	        return studentProjectService.listAll();
	    }
	 

	 @RequestMapping("/studentproject/show/{id}")
	    public StudentProject getStudentProject(@PathVariable Long id, Model model){
	        model.addAttribute("product", studentProjectService.getById(Long.valueOf(id)));
	        return studentProjectService.getById(id) ;
	    }
	 
	 
	 @RequestMapping(method = RequestMethod.PUT,value = "/studentproject/{id}")
	    public void updateStudentProject(@RequestBody StudentProject studentProject,@PathVariable Long id) {
		 
		 studentProjectService.saveOrUpdate(studentProject);
		 
	    	
	    }
	 
	 
	 
	 @RequestMapping(method = RequestMethod.POST,value = "/studentproject/add")
	    public void addStudentProject(@RequestBody StudentProject studentProject  ){
		 
		 studentProjectService.saveOrUpdate(studentProject);
		 
	 }
	 
	 @RequestMapping(method = RequestMethod.DELETE,value ="/studentproject/{id}")
	    public void delete(@PathVariable Long id){
		
	          studentProjectService.delete(id);
	    }
	

}
