package com.example.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.commands.Form;
import com.example.demo.converters.FormToStudentProject;
import com.example.demo.domain.StudentProject;
import com.example.demo.repositories.StudentProjectRepository;

@Service
public class StudentProjectServiceImpl implements StudentProjectService {
	
	
	private StudentProjectRepository studentProjectRepository;
	private FormToStudentProject formToStudentProject;
	
	@Autowired
	public StudentProjectServiceImpl(StudentProjectRepository studentProjectRepository,
			FormToStudentProject formToStudentProject) {
		
		this.studentProjectRepository = studentProjectRepository;
		this.formToStudentProject = formToStudentProject;
	}
	
	
	 @Override
	    public List<StudentProject> listAll() {
	        List<StudentProject> studentProject = new ArrayList<>();
	        studentProjectRepository.findAll().forEach(studentProject::add); //fun with Java 8
	        return studentProject;
	    }
	    
	 @Override
	    public StudentProject getById(Long id) {
	        return studentProjectRepository.findById(id).orElse(null);
	    }
	 @Override
	    public StudentProject saveOrUpdate(StudentProject studentProject) {
	        studentProjectRepository.save(studentProject);
	        return studentProject;
	    }
	

	@Override
	public void delete(Long id) {
	    
	     studentProjectRepository.deleteById(id);
		    
	    }
	
	 @Override 
	    public StudentProject saveOrUpdateForm(Form form) {
	    	StudentProject savedStudentProject = saveOrUpdate(formToStudentProject.convert(form));

	        System.out.println("Saved StudentProject Id: " + savedStudentProject.getId());
	        return savedStudentProject;
	    }
	

}
