package com.example.demo.services;

import java.util.List;

import com.example.demo.commands.Form;
import com.example.demo.domain.StudentProject;

public interface StudentProjectService {
	
	List<StudentProject> listAll();

    StudentProject getById(Long id);

    StudentProject saveOrUpdate(StudentProject studentProject);

   void delete(Long id);

    StudentProject saveOrUpdateForm(Form form);

	//void delete(String id);
    
  

}
