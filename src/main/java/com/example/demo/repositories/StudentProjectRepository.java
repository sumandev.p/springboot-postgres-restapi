package com.example.demo.repositories;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.domain.StudentProject;

public interface StudentProjectRepository extends CrudRepository<StudentProject, Long> {

}
